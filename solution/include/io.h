#include <stdio.h>

FILE* open(char* way, char* premis);

int close(FILE *file);

enum io_status  {
    OPEN_OK = 0,
    CLOSE_OK,
    OPEN_ERROR,
    CLOSE_ERROR
};
