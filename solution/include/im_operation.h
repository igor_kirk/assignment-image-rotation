#ifndef IM_OPERATION_H
#define IM_OPERATION_H

#include <image.h>

void image_destroy(struct image *img);

void create_image(uint32_t width, uint32_t height, struct image *img);

uint32_t count_padding(uint32_t width);

#endif
