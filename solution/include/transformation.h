#ifndef TRANSFORMATION_H
#define TRANSFORMATION_H

#include "image.h"

struct image rotate( struct image const *source );

#endif
