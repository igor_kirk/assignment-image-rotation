#ifndef CONSTANS_H
#define CONSTANS_H

#include <bmp.h>
#include <im_operation.h>
#include <image.h>
#include <inttypes.h>

uint16_t const Bformat = 0x4D42;
uint32_t Header_size = sizeof(struct bmp_header);
uint32_t BiSize = 40;
uint16_t BiBitCount = 24;
uint16_t BiPlanes = 1;

#endif
