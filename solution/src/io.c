#include <stdio.h>

FILE* open(char* way, char* premis) {
    FILE *file = fopen(way, premis);
    if (!file){
        printf("could not open the file");
        return NULL;
    }
    else{
        return file;
    }
}

int close(FILE *file) {
    int err = fclose(file);
    if (err){
        printf("could not close the file");
        return 1;
    }
    else{
        return 0;
    }
}
