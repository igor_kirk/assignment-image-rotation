#include "image.h"
#include "bmp.h"
#include "im_operation.h"
#include "io.h"
#include "transformation.h"
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

int main( int argc, char** argv ) {
    if (!(argc == 3)) {
        printf("invalid input parameteres");
        return 1;
    } 
    FILE *in;
    FILE *out;
    in = open(argv[1], "rb");
    if (!in) return OPEN_ERROR;

    out = open(argv[2], "wb");
    if (!out) return OPEN_ERROR;

    struct image img;
    if (from_bmp(in, &img)) {
        printf("from_bmp error");
        return from_bmp(in, &img);
    }
    struct image rotated = rotate(&img);
    if (to_bmp(out, &rotated)) {
        printf("to_bmp error");
        return to_bmp(in, &rotated);
    }

    image_destroy(&img);
    image_destroy(&rotated);

    if (close(in)) return CLOSE_ERROR;
    if (fclose(out)) return CLOSE_ERROR;


    return 0;
}
