#include "image.h"
#include "im_operation.h"
#include "stdlib.h"

struct image rotate( struct image const *source ) {
    struct image img;
    create_image(source->height, source->width, &img);

    uint32_t cur_value;
    for (uint32_t y = 0; y < img.height; y++){
        cur_value = img.height * (img.width - 1) + y;
        for (uint32_t x = 0; x < img.width; x++){
            img.data[y*img.width + x] = source->data[cur_value];
            cur_value = cur_value - img.height;
        }
    }

    return img;
}
