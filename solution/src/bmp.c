#include "image.h"
#include "bmp.h"
#include "constans.h"
#include "im_operation.h"
#include <stdlib.h>

void image_destroy(struct image *img) {
  free(img->data);
  img->height = 0;
  img->width = 0;
}

void create_image(uint32_t width, uint32_t height, struct image *img) {
    if (img == NULL) return;
    img->height = height;
    img->width = width;
    img->data = malloc(sizeof(struct pixel) * width * height);
}

uint32_t count_padding(uint32_t width) {
    uint32_t a = 4 - ((width * 3) % 4);
    return ((width * 3) % 4) ? a : 0;
}

enum read_status from_bmp(FILE *in, struct image *img) {
    if (in == NULL || img == NULL) {
        printf("invalid image or input file");
        return READ_ERROR;
    }

    struct bmp_header header;

    if (!fread(&header, sizeof(struct bmp_header), 1, in)) return READ_INVALID_HEADER;
    if (header.bfType != Bformat) return READ_INVALID_SIGNATURE;

    if (fseek(in, (uint16_t)header.bOffBits, SEEK_SET)) return READ_ERROR;

    create_image(header.biWidth, header.biHeight, img);

    uint32_t padding = count_padding(img->width);
    uint32_t width_in_bytes = img->width * 3;

    for (uint32_t y = 0; y < img->height; y++) {
        if (!(fread(img->data + y*img->width, 1, width_in_bytes, in))) return READ_ERROR;
        if (fseek(in, (uint16_t)padding, SEEK_CUR)) return READ_ERROR;
    }

    return READ_OK;

}

static struct bmp_header build_header(uint32_t width, uint32_t height) {
    struct bmp_header header;

    uint32_t padding = count_padding(width);
    uint32_t const Filesize = sizeof(struct bmp_header) + (3 * width + padding) * height;
    uint32_t const Imagesize = (3 * width + padding) * height;

    header.bfType = Bformat;
    header.bfileSize = Filesize;
    header.bfReserved = 0;
    header.bOffBits = Header_size;
    header.biSize = BiSize;
    header.biWidth = width;
    header.biHeight = height;
    header.biPlanes = BiPlanes;
    header.biBitCount = BiBitCount;
    header.biCompression = 0;
    header.biSizeImage = Imagesize;
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;
    return header;
}

enum write_status to_bmp(FILE *out, struct image const *img){
    if (out == NULL || img == NULL) {
        printf("invalid image or output file");
        return WRITE_ERROR;
    }

    struct bmp_header header = build_header(img->width, img->height);
    if (!(fwrite(&header, sizeof(struct bmp_header), 1, out))) return WRITE_ERROR;

    uint32_t padding = count_padding(img->width);
    uint32_t width_in_bytes = img->width * 3;

    for (uint32_t y = 0; y < img->height; y++) {
        if (!fwrite(img->data + y*img->width, width_in_bytes, 1, out)) return WRITE_ERROR;
        if (fseek(out, (uint16_t)padding, SEEK_CUR)) return WRITE_ERROR;
    }

    return WRITE_OK;
}
